// Package exp is experimental and may change without warning.
package exp

import (
	"gitlab.com/accumulatenetwork/accumulate/internal/core"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/merkle"
)

type GlobalValues = core.GlobalValues

type Receipt = merkle.Receipt
type ReceiptEntry = merkle.ReceiptEntry
